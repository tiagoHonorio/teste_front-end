import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from "rxjs/Observable";
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const url = 'http://localhost:3000/';

@Injectable()
export class TaskService {

  constructor(private http: HttpClient) { }


  listTaks(): Observable<object> {
    return this.http.get(url+'tasks/list');
  }

  saveBd(data): Observable<object> {
    const body = JSON.stringify(data);
    return this.http.post(url+'tasks', body, httpOptions);
  }

  updateBd(data): Observable<object> {
    const body = JSON.stringify(data);
    return this.http.put(url+'tasks/'+data.id, body, httpOptions);
  }
  deleteDb(data):Observable<object> {
    return this.http.delete(url+'tasks/'+data.id);
  }
}
