import { Component, OnInit } from '@angular/core';
import {TaskService} from './task.service';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {


  constructor(private tkServe: TaskService) { }
  tilulo = "Nova Task!";
  tasks = '';
  modell = {id: '', title : '', description: ''};

  list() {
    this.tkServe.listTaks()
      .subscribe(res => {
        let resp = res;
        if(resp.success){
          this.tasks = resp.data;
        }else{
          this.tasks = [];
        }

        console.log(this.tasks);
      });
  }

  openModal(model):any {
    this.modell = model || {id: '', title : '', description: ''} ;
    console.log(this.modell);
  }

  onDelete(data) {
    if (confirm("Tem certeza?")) {
      this.tkServe.deleteDb(data)
        .subscribe(res => {

        });
    }
  }

  saveTask(data) {
    if (!data.id){
      this.tkServe.saveBd(data)
        .subscribe(res => {
          const resp = res;
          if (resp.success){
            data.id = res.data.insertId;
            this.tasks.push(data);
          }
        });
    } else {
      this.tkServe.updateBd(data)
        .subscribe(res => {
        });
    }

  }
  ngOnInit() {
    this.list();
  }

}
